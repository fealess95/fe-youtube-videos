import React, { PropsWithChildren } from 'react';
import { render, RenderOptions } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import thunk from 'redux-thunk';
import { PreloadedState } from '@reduxjs/toolkit';
import setupStore, { AppStore, RootState } from '../store';

interface Options extends Omit<RenderOptions, 'queries'> {
  initState: any;
}
export const renderWithApp = (ui: React.ReactElement, options?: Options) => {
  const middlewares = [thunk];
  const mockStore = configureStore(middlewares);
  const store = mockStore(options?.initState || {});

  const Wrapper = ({ children }: any) => (
    <Provider store={store}>
      <BrowserRouter>{children}</BrowserRouter>
    </Provider>
  );

  return {
    ...render(ui, { ...options, wrapper: Wrapper }),
    store,
  };
};

export const renderWithRedux = (ui: React.ReactElement, options?: Options) => {
  const middlewares = [thunk];
  const mockStore = configureStore(middlewares);
  const store = mockStore(options?.initState || {});

  const Wrapper = ({ children }: any) => (
    <Provider store={store}>{children}</Provider>
  );

  return {
    ...render(ui, { ...options, wrapper: Wrapper }),
    store,
  };
};

interface ExtendedRenderOptions extends Omit<RenderOptions, 'queries'> {
  preloadedState?: PreloadedState<RootState>;
  store?: AppStore;
}

export function renderWithProviders(
  ui: React.ReactElement,
  {
    preloadedState = {},
    // Automatically create a store instance if no store was passed in
    store = setupStore(preloadedState),
    ...renderOptions
  }: ExtendedRenderOptions = {}
) {
  function Wrapper({ children }: PropsWithChildren<{}>): JSX.Element {
    return (
      <Provider store={store}>
        <BrowserRouter>{children}</BrowserRouter>
      </Provider>
    );
  }
  return { store, ...render(ui, { wrapper: Wrapper, ...renderOptions }) };
}
