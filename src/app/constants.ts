export const ROUTES = {
  home: '/',
  share: '/share',
};
export const COOKIE_KEYS = {
  token: 'token',
  username: 'username',
};
export const API_DOMAIN = 'https://be-youtube-videos.herokuapp.com';
export const API_STATUSES = {
  loading: 'loading',
  succeeded: 'succeeded',
  failed: 'failed',
};
